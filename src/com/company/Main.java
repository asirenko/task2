package com.company;

import java.math.BigInteger;

public class Main {

    public static void main(String[] args) {
        System.out.println(String.valueOf(factorial(BigInteger.valueOf(20))));
    }

    public static BigInteger factorial(BigInteger n)
    {
        if (n.equals(BigInteger.ZERO)) return BigInteger.ONE;
        return n.multiply(factorial(n.subtract(BigInteger.ONE)));
    }
}
